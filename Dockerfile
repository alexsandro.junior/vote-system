FROM jdk-11

VOLUME /tmp
COPY target/*.jar /app.jar

RUN mvn clean package -B
RUN docker-compose up --build

EXPOSE 8080
ENTRYPOINT java -jar app.jar