package com.avenuecode.vote.utils;

import com.avenuecode.vote.model.Associate;
import com.avenuecode.vote.model.Schedule;
import com.avenuecode.vote.model.Session;
import com.avenuecode.vote.model.Vote;
import org.springframework.stereotype.Component;

import java.util.Calendar;

@Component
public class InsertingUtils {

    public Associate addAssociate() {
        Associate associate = new Associate();
        associate.setId("1");
        associate.setCpf("32657021034");
        associate.setName("João");
        return associate;
    }

    public Schedule addSchedule() {
        Schedule schedule = new Schedule();
        schedule.setId("1");
        schedule.setDescription("Digital Freedom?");
        return schedule;
    }

    public Session addSession() {
        Session session = new Session();
        session.setId("1");
        session.setIdSchedule("1");
        Calendar calendar = Calendar.getInstance();
        session.setStart(calendar);
        return session;
    }

    public Vote addVote() {
        Vote vote = new Vote();
        vote.setId("1");
        vote.setCpf("32657021034");
        vote.setIdSchedule("1");
        vote.setOption(true);
        return vote;
    }

}
