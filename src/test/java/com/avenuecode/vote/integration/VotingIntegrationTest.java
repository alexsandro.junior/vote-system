package com.avenuecode.vote.integration;

import com.avenuecode.vote.model.Associate;
import com.avenuecode.vote.model.Schedule;
import com.avenuecode.vote.model.Vote;
import com.avenuecode.vote.repository.AssociateRepository;
import com.avenuecode.vote.repository.VoteRepository;
import com.avenuecode.vote.service.AssociateService;
import com.avenuecode.vote.service.ScheduleService;
import com.avenuecode.vote.service.VoteService;
import com.avenuecode.vote.utils.InsertingUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
@RunWith(SpringRunner.class)
public class VotingIntegrationTest {

    @Autowired
    private InsertingUtils insertingUtils;

    @Autowired
    private AssociateService associateService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private VoteService voteService;

    @Autowired
    private AssociateRepository associateRepository;

    @Autowired
    private VoteRepository voteRepository;

    @Test
    void showAssociates() {
        List<Associate> allAssociates = associateService.getAllAssociates();
        assertThat(allAssociates).size().isGreaterThan(0);
    }

    @Test
    @Rollback(value = false)
    void findAssociateByName() {
        String cpf = "32657021034";
        Optional<Associate> associate = associateRepository.findByCpf(cpf);
        assertThat(associate.orElse(null)).isNotNull();
    }

    @Test
    public void testVote() {
        List<Vote> votes = new ArrayList<>();
        votes.add(insertingUtils.addVote());
        voteService.addVotes(votes);
    }


    @Test
    void findSessionByNameNotExistTest () {
        String description = "Not Exists";
        List<Schedule> scheduleList = scheduleService.getAllSchedules();
        if(!scheduleList.isEmpty()){
            List<Schedule> schedulesWithSameDescription = scheduleList
                    .stream()
                    .filter(s -> s.getDescription().equals(description))
                    .collect(Collectors.toList());
            assertNotEquals(true, schedulesWithSameDescription.isEmpty());
        }
    }


    @Test
    @Rollback(value = false)
    void deleteExistingVote() {
        String id = "99";
        Vote vote = new Vote("11122233300", "99", true);
        vote.setId(id);
        voteRepository.save(vote);

        boolean isPresentBeforeDelete = voteRepository.findById(id).isPresent();
        voteRepository.deleteById(id);
        boolean notPresentAfterDelete = voteRepository.findById(id).isPresent();
        assertTrue(isPresentBeforeDelete);
        assertFalse(notPresentAfterDelete);
    }
}
