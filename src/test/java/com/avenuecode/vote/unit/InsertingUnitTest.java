package com.avenuecode.vote.unit;


import com.avenuecode.vote.model.Associate;
import com.avenuecode.vote.model.Schedule;
import com.avenuecode.vote.model.Session;
import com.avenuecode.vote.model.Vote;
import com.avenuecode.vote.repository.AssociateRepository;
import com.avenuecode.vote.repository.ScheduleRepository;
import com.avenuecode.vote.repository.SessionRepository;
import com.avenuecode.vote.repository.VoteRepository;
import com.avenuecode.vote.utils.InsertingUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class InsertingUnitTest {

    @Autowired
    private InsertingUtils insertingUtils;

    @MockBean
    private AssociateRepository associateRepository;

    @MockBean
    private ScheduleRepository scheduleRepository;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private VoteRepository voteRepository;

    @Test
    public void testInsertAssociate(){
        Associate associate = insertingUtils.addAssociate();
        Mockito.when(associateRepository.save(associate)).thenReturn(associate);
        assertEquals("João", associate.getName());
    }

    @Test
    public void testInsertSchedule(){
        Schedule schedule = insertingUtils.addSchedule();
        Mockito.when(scheduleRepository.save(schedule)).thenReturn(schedule);
        assertEquals("Digital Freedom?", schedule.getDescription());
    }

    @Test
    public void testInsertSession(){
        Session session = insertingUtils.addSession();
        Mockito.when(sessionRepository.save(session)).thenReturn(session);
        assertEquals("1", session.getIdSchedule());
    }

    @Test
    public void testInsertVote(){
        Vote vote = insertingUtils.addVote();
        Mockito.when(voteRepository.save(vote)).thenReturn(vote);
        assertEquals(true, vote.getOption());
    }

}
