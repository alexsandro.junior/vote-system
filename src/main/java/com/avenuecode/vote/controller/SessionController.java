package com.avenuecode.vote.controller;

import com.avenuecode.vote.model.Session;
import com.avenuecode.vote.service.SessionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log
@RestController
@RequiredArgsConstructor
@RequestMapping("/session")
public class SessionController {

    @Autowired
    SessionService sessionService;

    @PostMapping("/add")
    public ResponseEntity<List<Session>> addSessions(@RequestBody List<Session> sessionList) {
        List<Session> response = new ArrayList<>();
        log.info(String.format("POST /session/add"));
        response.addAll(sessionService.addSessions(sessionList));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/get-session")
    public ResponseEntity<List<Session>> getAllSession() {
        log.info(String.format("GET /session/get-session"));
        List<Session> response = sessionService.getAllSession();
        if (!response.isEmpty()) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/find-session-idschedule/{idSchedule}")
    public ResponseEntity<Session> findByIdSchedule(@PathVariable String idSchedule) {
        log.info(String.format("GET /session/find-session-idschedule/{idSchedule}"));
        Optional<Session> response = sessionService.findByIdSchedule(idSchedule);
        if (response.isPresent()) {
            return new ResponseEntity<>(response.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete-all-sessions")
    public ResponseEntity<HttpStatus> deleteAll() {
        log.info(String.format("DELETE /session/delete-all-sessions"));
        try {
            sessionService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
