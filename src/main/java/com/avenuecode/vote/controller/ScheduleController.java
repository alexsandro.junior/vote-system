package com.avenuecode.vote.controller;

import com.avenuecode.vote.model.Schedule;
import com.avenuecode.vote.service.ScheduleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Log
@RestController
@RequiredArgsConstructor
@RequestMapping("/schedule")
public class ScheduleController {

    @Autowired
    ScheduleService scheduleService;


    @PostMapping("/add")
    public ResponseEntity<List<Schedule>> addSchedules(@RequestBody List<Schedule> scheduleList) {
        List<Schedule> response = new ArrayList<>();
        log.info(String.format("POST /schedule/add"));

        response.addAll(scheduleService.addSchedules(scheduleList));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/get-schedules")
    public ResponseEntity<List<Schedule>> getAllSchedules() {
        log.info(String.format("GET /schedule/get-schedules"));
        List<Schedule> response = scheduleService.getAllSchedules();

        if (!response.isEmpty()) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/delete-all-schedules")
    public ResponseEntity<HttpStatus> deleteAll() {
        log.info(String.format("DELETE /schedule/delete-all-schedules"));
        try {
            scheduleService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
