package com.avenuecode.vote.controller;

import com.avenuecode.vote.model.Vote;
import com.avenuecode.vote.service.VoteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log
@RestController
@RequiredArgsConstructor
@RequestMapping("/voting")
public class VotingController {

    @Autowired
    VoteService voteService;


    @PostMapping("/do-vote")
    public ResponseEntity<List<Vote>> addVotes(@RequestBody List<Vote> votes) {
        List<Vote> response = new ArrayList<>();
        log.info(String.format("POST /voting/do-vote"));

        response.addAll(voteService.addVotes(votes));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/get-vote")
    public ResponseEntity<List<Vote>> getAllVotes() {
        List<Vote> response = voteService.getAllVotes();
        log.info(String.format("GET /voting/get-vote"));

        if (!response.isEmpty()) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/find-vote-idschedule/{idSchedule}")
    public ResponseEntity<Vote> findByIdSchedule(@PathVariable String idScheduleVote) {
        log.info(String.format("GET /voting/find-vote-idschedule/{cpf}"));
        Optional<Vote> response = voteService.findByIdSchedule(idScheduleVote);

        if (response.isPresent()) {
            return new ResponseEntity<>(response.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete-all-votes")
    public ResponseEntity<HttpStatus> deleteAll() {
        log.info(String.format("DELETE /voting/delete-all-votes"));

        try {
            voteService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
