package com.avenuecode.vote.controller;

import com.avenuecode.vote.model.Associate;
import com.avenuecode.vote.service.AssociateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log
@RestController
@RequiredArgsConstructor
@RequestMapping("/associate")
public class AssociateController {

    @Autowired
    AssociateService associateService;


    @PostMapping("/add")
    public ResponseEntity<List<Associate>> addAssociates(@RequestBody List<Associate> associateList) {
        List<Associate> response = new ArrayList<>();
        log.info(String.format("POST /associate/add"));

        response.addAll(associateService.addAssociates(associateList));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/get-associates")
    public ResponseEntity<List<Associate>> getAllAssociates() {
        log.info(String.format("GET /associate/get-associates"));
        List<Associate> response = associateService.getAllAssociates();

        if (!response.isEmpty()) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/find-cpf/{cpf}")
    public ResponseEntity<Associate> findByCpf(@PathVariable String cpf) {
        log.info(String.format("GET /associate/find-cpf/{cpf}"));
        Optional<Associate> response = associateService.findByCpf(cpf);

        if (response.isPresent()) {
            return new ResponseEntity<>(response.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete-all-associates")
    public ResponseEntity<HttpStatus> deleteAll() {
        log.info(String.format("DELETE /associate/delete-all-associates"));
        try {
            associateService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
