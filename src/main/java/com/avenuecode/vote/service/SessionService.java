package com.avenuecode.vote.service;

import com.avenuecode.vote.model.Schedule;
import com.avenuecode.vote.model.Session;
import com.avenuecode.vote.repository.ScheduleRepository;
import com.avenuecode.vote.repository.SessionRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Log
@Service
public class SessionService {

    @Autowired
    SessionRepository sessionRepository;

    public List<Session> getAllSession() {
        log.info(String.format("SessionService - getAllSessions()) - Getting all sessions"));
        return sessionRepository.findAll();
    }

    public Optional<Session> findByIdSchedule(String idSchedule) {
        log.info(String.format("SessionService - findByIdSchedule()) - Getting session by id %s", idSchedule));
        return sessionRepository.findByIdSchedule(idSchedule);
    }

    public void deleteAll() {
        log.info(String.format("SessionService - deleteAll()) - Deleting all sessions"));
        sessionRepository.deleteAll();
    }

    public List<Session> addSessions(List<Session> sessionList) {
        sessionList.forEach(session -> {
            Calendar calendar = Calendar.getInstance();
            session.setStart(calendar);
        });
        log.info(String.format("SessionService - addSessions()) - Adding sessions"));
        return sessionRepository.saveAll(removeDuplicateSessions(sessionList));
    }

    private List removeDuplicateSessions(List<Session> unique) {
        Map<String, Session> amap = new HashMap<String, Session>();
        for(Session i: unique){
            amap.put(i.getIdSchedule(), i);
        }
        List<Session> newSessionList = new ArrayList(amap.values());
        log.info(String.format("SessionService - removeDuplicateSessions()) - Removing duplicated sessions"));
        return newSessionList;
    }

}
