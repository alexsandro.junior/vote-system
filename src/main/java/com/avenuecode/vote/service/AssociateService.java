package com.avenuecode.vote.service;

import com.avenuecode.vote.model.Associate;
import com.avenuecode.vote.repository.AssociateRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Log
@Service
public class AssociateService {

    @Autowired
    AssociateRepository associateRepository;


    public List<Associate> getAllAssociates() {
        log.info(String.format("AssociateService - getAllAssociates()) - Getting all associates"));
        return associateRepository.findAll();
    }

    public Optional<Associate> findByCpf(String cpf) {
        log.info(String.format("AssociateService - findByCpf()) - Getting associates by cpf %s",cpf));
        return associateRepository.findByCpf(cpf);
    }

    public void deleteAll() {
        log.info(String.format("AssociateService - deleteAll()) - Deleting all associates"));
        associateRepository.deleteAll();
    }

    public List<Associate> addAssociates(List<Associate> associateList) {
        log.info(String.format("(AssociateService - addAssociates()) - Saving %s associates", associateList.size()));
        return associateRepository.saveAll(associateList);
    }
}
