package com.avenuecode.vote.service;

import com.avenuecode.vote.model.Session;
import com.avenuecode.vote.model.Vote;
import com.avenuecode.vote.repository.SessionRepository;
import com.avenuecode.vote.repository.VoteRepository;
import lombok.extern.java.Log;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

@Log
@Service
public class VoteService {

    @Autowired
    VoteRepository voteRepository;

    @Autowired
    SessionRepository sessionRepository;

    String myURL = ("https://user-info.herokuapp.com/users/");

    public List<Vote> getAllVotes() {
        log.info(String.format("VoteService - getAllVotes()) - Getting all votes"));
        return voteRepository.findAll();
    }

    public Optional<Vote> findByIdSchedule(String idScheduleVote) {
        log.info(String.format("VoteService - findByIdSchedule()) - Getting vote by id %s",idScheduleVote));
        return voteRepository.findByIdSchedule(idScheduleVote);
    }

    public void deleteAll() {
        log.info(String.format("VoteService - deleteAll()) - Deleting all votes"));
        voteRepository.deleteAll();
    }

    public List<Vote> addVotes(List<Vote> votes) {
        List<Vote> upVotes = new ArrayList<>();

        List<Session> sessionAll = sessionRepository.findAll();

        if(!votes.isEmpty()){
            votes.forEach(vote -> {
                try {
                    String result = checkCPF(myURL + vote.getCpf());
                    if(result.equals("ABLE_TO_VOTE")){
                        if (!sessionAll.isEmpty()) {
                            sessionAll.forEach(session -> {
                                if(session.getIdSchedule().equals(vote.getIdSchedule())){
                                    Date dateNow = new Date();
                                    Vote newVote = new Vote(vote.getCpf(), vote.getIdSchedule(), vote.getOption());
                                    if (checkTimeSession(dateNow, session.getStart())) {
                                        upVotes.add(newVote);
                                    } else {
                                        log.info(String.format("VoteService - addVotes()) - The vote %s passed the time limit", vote.getId()));
                                    }
                                }
                            });
                        }
                    }else{
                        log.info(String.format("(VoteService - addVotes()) - The associate with %s is not able to vote", vote.getCpf()));
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        if(checkAlreadyVoted(upVotes)){
            return upVotes;
        }else{
            return voteRepository.saveAll(upVotes);
        }
    }

    public static String checkCPF(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        }
        JSONObject jsonObj = new JSONObject(result.toString());
        return jsonObj.get("status").toString();
    }

    private boolean checkTimeSession(Date dateNow, Calendar startDate) {
        long timeInSecs = startDate.getTimeInMillis();
        Date afterAdding1Min = new Date(timeInSecs + (1 * 60 * 1000));
        if(dateNow.after(startDate.getTime()) && dateNow.before(afterAdding1Min)){
            return true;
        }else{
            return false;
        }
    }

    private boolean checkAlreadyVoted(List<Vote> upVotes) {
        List<Vote> votes = voteRepository.findAll();
        boolean found = false;
        if(!votes.isEmpty()){
            for(Vote vote : votes){
                for(Vote upVotesMatch: upVotes){
                    if(vote.getCpf().equals(upVotesMatch.getCpf()) && vote.getIdSchedule().equals(upVotesMatch.getIdSchedule())){
                        found = true;
                        upVotes.remove(vote);
                        log.info(String.format("VoteService - checkAlreadyVoted()) - The associate with cpf:%s has already voted in this session", vote.getCpf()));
                        break;
                    }
                }
            }
        }
        return found;
    }

}
