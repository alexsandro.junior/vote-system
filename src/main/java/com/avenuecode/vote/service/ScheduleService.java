package com.avenuecode.vote.service;

import com.avenuecode.vote.model.Schedule;
import com.avenuecode.vote.repository.ScheduleRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Log
@Service
public class ScheduleService {

    @Autowired
    ScheduleRepository scheduleRepository;

    public List<Schedule> getAllSchedules() {
        log.info(String.format("ScheduleService - getAllSchedules()) - Getting all schedules"));
        return scheduleRepository.findAll();
    }

    public void deleteAll() {
        log.info(String.format("ScheduleService - deleteAll()) - Deleting all schedules"));
        scheduleRepository.deleteAll();
    }

    public List<Schedule> addSchedules(List<Schedule> scheduleList) {
        log.info(String.format("(ScheduleService - addSchedules()) - Saving %s schedules", scheduleList.size()));
        return scheduleRepository.saveAll(scheduleList);
    }
}
