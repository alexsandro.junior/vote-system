package com.avenuecode.vote.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@NoArgsConstructor
public class Vote {

    @Id
    private String id;
    private String cpf;
    private String idSchedule;
    private Boolean option;



    public Vote(String cpf, String idSchedule, Boolean option) {
        this.cpf = cpf;
        this.idSchedule = idSchedule;
        this.option = option;
    }
}
