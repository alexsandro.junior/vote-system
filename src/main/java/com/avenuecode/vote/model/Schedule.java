package com.avenuecode.vote.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@NoArgsConstructor
public class Schedule {

    @Id
    private String id;

    private String description;

    public Schedule(String description) {
        this.description = description;
    }
}
