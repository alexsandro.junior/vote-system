package com.avenuecode.vote.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@NoArgsConstructor
public class Associate {

    @Id
    private String id;

    private String name;

    private String cpf;

    public Associate(String name, String cpf) {
        this.name = name;
        this.cpf = cpf;
    }
}
