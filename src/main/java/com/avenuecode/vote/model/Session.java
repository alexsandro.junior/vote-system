package com.avenuecode.vote.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.util.Calendar;

@Getter
@Setter
@NoArgsConstructor
public class Session {

    @Id
    private String id;
    private String idSchedule;
    private Calendar start;

    public Session(String idSchedule, Calendar start) {
        this.idSchedule = idSchedule;
        this.start = start;
    }
}
