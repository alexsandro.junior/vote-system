package com.avenuecode.vote.repository;

import com.avenuecode.vote.model.Session;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SessionRepository extends MongoRepository<Session, String> {
    Optional<Session> findByIdSchedule(String idSchedule);
}
