package com.avenuecode.vote.repository;

import com.avenuecode.vote.model.Vote;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VoteRepository extends MongoRepository<Vote, String> {
    Optional<Vote> findByIdSchedule(String idScheduleVote);
}
